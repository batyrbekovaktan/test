<?php

namespace App\Tests;


use App\Entity\User;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersTest extends AbstractEndPoint
{

    private $em;
    /**
     * @return void
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    private string $userPayload = '{"email": "%s", "password": "password"}';

    /**
     * @return string
     */
    private function getPayload(): string
    {
        $faker = Factory::create();
        return sprintf($this->userPayload, $faker->email);
    }

    /**
     * @return void
     */
    public function testNotAuthUserCanSeeAllUsers(): void
    {
        $response = $this->getResponseFromRequest(Request::METHOD_GET, '/api/users');

        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJson($responseContent);
        $this->assertNotEmpty($responseDecoder);
    }

    /**
     * @return void
     */
    public function testNotAuthUserCanSeeOneUser(): void
    {
        $user = $this->em->getRepository(User::class)->findOneBy([]);

        $response = $this->getResponseFromRequest(Request::METHOD_GET, '/api/users/' . $user->getId());
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertJson($responseContent);
        $this->assertNotEmpty($responseDecoder);
    }

    /**
     * @return void
     */
    public function testNotAuthUserCanCreateUser(): void
    {
        $response = $this->getResponseFromRequest(
            Request::METHOD_POST,
            '/api/users',
            $this->getPayload()

        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent);

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $this->assertJson($responseContent);
        $this->assertNotEmpty($responseDecoder);
    }

    /**
     * @return void
     */
    public function testUserCanNotCreateUserWithoutEmail(): void
    {
        $client = static::createClient();

        $response = $client->request('GET', '/api/products');

        $this->assertResponseStatusCodeSame(200);
    }
}
