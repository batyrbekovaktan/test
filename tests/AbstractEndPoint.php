<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AbstractEndPoint extends WebTestCase
{
    public array $serverInformation = ['ACCEPT' => 'application/json', 'CONTENT_TYPE' => 'application/json'];

    /**
     * @param $method
     * @param $uri
     * @param string $payload
     * @return Response
     */
    public function getResponseFromRequest($method, $uri, string $payload = ''): Response
    {
        $client = self::createClient();

        $client->request($method,
            $uri . '.json',
            [],
            [],
            $this->serverInformation,
            $payload
        );

        return $client->getResponse();
    }

}