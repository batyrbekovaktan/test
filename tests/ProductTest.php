<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Product;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProductTest extends ApiTestCase
{
    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testNotAuthUserCanSeeAllProducts(): void
    {
        $response = static::createClient()->request('GET', '/api/products', [
            'headers' => [
                'ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            ]]);
        $responseDecode = json_decode($response->getContent());

        $this->assertResponseStatusCodeSame(200);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function testNotAuthUserCanSeeOneProduct(): void
    {
        $product_api = $this->findIriBy(Product::class, []);

        static::createClient()->request('GET', $product_api, [
            'headers' => [
                'ACCEPT' => 'application/json',
                'CONTENT_TYPE' => 'application/json',
            ]]);

        $this->isJson();
        $this->assertResponseStatusCodeSame(200);
    }


    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testUpdateProduct(): void
    {
        $client = static::createClient();
        $iri = $this->findIriBy(Product::class, []);

        $client->request('PUT', $iri, ['json' => [
            'name' => 'updated name',
        ]]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@id' => $iri,
            'name' => 'updated name',
        ]);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function testDeleteProduct(): void
    {
        $client = static::createClient();
        $iri = $this->findIriBy(Product::class, []);

        $client->request('DELETE', $iri);

        $this->assertResponseStatusCodeSame(204);
    }
}
