<?php

namespace App\Tests;


use App\Entity\Product;
use Faker\Factory;
use Symfony\Component\HttpFoundation\Request;

class ProductsTest extends AbstractEndPoint
{

    private $em;
    /**
     * @return void
     */
    protected function setUp(): void
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
        $faker = Factory::create();
        $this->name = $faker->word;
        $this->description = $faker->userName;
        $this->price = $faker->randomFloat();
        $this->payload = '{"name": "%s", "description": "%s", "price": "%d"}';
    }

    /**
     * @return void
     */
    public function testNotAuthUserCanCreateProduct(): void
    {
        $payload = sprintf($this->payload, $this->name, $this->description, $this->price);
        $response = $this->getResponseFromRequest(
            Request::METHOD_POST,
            '/api/products',
            $payload
        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent, true);

        $this->assertResponseStatusCodeSame(201);
        $this->assertArrayHasKey('id', $responseDecoder);
        $this->assertArrayHasKey('name', $responseDecoder);
        $this->assertArrayHasKey('description', $responseDecoder);
        $this->assertEquals($this->name, $responseDecoder['name']);
        $this->assertEquals($this->description, $responseDecoder['description']);
    }


    /**
     * @return void
     */
    public function testNotAuthUserCanNotCreateProductWithoutName(): void
    {
        $payload = '{"description": "%s", "price": "%d"}';
        $payload = sprintf($payload, $this->description, $this->price);
        $response = $this->getResponseFromRequest(
            Request::METHOD_POST,
            '/api/products',
            $payload
        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent, true);

        $this->assertResponseStatusCodeSame(422);
        $this->assertEquals('name: This value should not be null.', $responseDecoder['detail']);
    }

    /**
     * @return void
     */
    public function testNotAuthUserCanNotCreateProductWithNameLessThanTwoCharacters(): void
    {
        $payload = sprintf($this->payload, 's', $this->description, $this->price);
        $response = $this->getResponseFromRequest(
            Request::METHOD_POST,
            '/api/products',
            $payload
        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent, true);

        $this->assertResponseStatusCodeSame(422);
        $this->assertEquals('name: This value is too short. It should have 2 characters or more.', $responseDecoder['detail']);
    }


    /**
     * @return void
     */
    public function testNotAuthUserCanUpdateProduct(): void
    {
        $product = $this->em->getRepository(Product::class)->findOneBy([]);

        $payload = sprintf($this->payload, $this->name, $this->description, $this->price);
        $response = $this->getResponseFromRequest(
            Request::METHOD_PUT,
            '/api/products/' . $product->getId(),
            $payload
        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent, true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertArrayHasKey('id', $responseDecoder);
        $this->assertArrayHasKey('name', $responseDecoder);
        $this->assertArrayHasKey('description', $responseDecoder);
        $this->assertEquals($this->name, $responseDecoder['name']);
        $this->assertEquals($this->description, $responseDecoder['description']);
    }


    /**
     * @return void
     */
    public function testNotAuthUserCanSeeOneProduct(): void
    {
        $product = $this->em->getRepository(Product::class)->findOneBy([]);

        $response = $this->getResponseFromRequest(
            Request::METHOD_GET,
            '/api/products/' . $product->getId()
        );
        $responseContent = $response->getContent();
        $responseDecoder = json_decode($responseContent, true);

        $this->assertResponseStatusCodeSame(200);
        $this->assertEquals($product->getId(), $responseDecoder['id']);
        $this->assertEquals($product->getName(), $responseDecoder['name']);
        $this->assertEquals($product->getDescription(), $responseDecoder['description']);
    }


    /**
     * @return void
     */
    public function testUserCanDeleteProduct(): void
    {
        $product = $this->em->getRepository(Product::class)->findOneBy([]);

        $response = $this->getResponseFromRequest(
            Request::METHOD_DELETE,
            '/api/products/' . $product->getId()
        );

        $this->assertResponseStatusCodeSame(204);;
    }
}
