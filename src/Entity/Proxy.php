<?php

namespace App\Entity;

use App\Repository\ProxyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProxyRepository::class)]
class Proxy
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $proxy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProxy(): ?string
    {
        return $this->proxy;
    }

    public function setProxy(string $proxy): self
    {
        $this->proxy = $proxy;

        return $this;
    }
}
