<?php

namespace App\Entity;

use App\Repository\ImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use DateTime;

#[ORM\Entity(repositoryClass: ImageRepository::class)]
#[Vich\Uploadable]
class Image
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $image;

    #[Vich\UploadableField(mapping: 'images', fileNameProperty: 'image')]
    private mixed $imageFile;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $updatedAt;

    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'images')]
    private $product;

    #[ORM\OneToOne(mappedBy: 'image', targetEntity: Mini::class, cascade: ['persist', 'remove'])]
    private $mini;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPictureFile(): mixed
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setPictureFile($imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getMini(): ?Mini
    {
        return $this->mini;
    }

    public function setMini(Mini $mini): self
    {
        // set the owning side of the relation if necessary
        if ($mini->getImage() !== $this) {
            $mini->setImage($this);
        }

        $this->mini = $mini;

        return $this;
    }
}
