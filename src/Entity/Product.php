<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
#[ApiResource(normalizationContext: ['groups' => ['read', 'attributes']])]
#[UniqueEntity('name')]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['attributes'])]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['attributes'])]
    #[Assert\NotNull]
    #[Assert\Length(min: 2, max: 255)]
    private ?string $name;

    #[ORM\Column(type: 'string')]
    #[Groups(['attributes'])]
    #[Assert\NotBlank]
    private ?float $price;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['attributes'])]
    #[Assert\NotNull]
    #[Assert\Length(min: 3)]
    private $description;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'products')]
    #[Groups(['attributes'])]
    private $category;

    #[Groups(['attributes'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $picture;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: OrderProduct::class)]
    #[Groups(['attributes'])]
    private $orderProducts;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Image::class)]
    private $images;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Comment::class)]
    private $comments;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Grade::class)]
    private $grades;

    #[ORM\OneToMany(mappedBy: 'product', targetEntity: Imagick::class, orphanRemoval: true)]
    private $imagicks;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->grades = new ArrayCollection();
        $this->imagicks = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setProduct($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->removeElement($orderProduct)) {
            if ($orderProduct->getProduct() === $this) {
                $orderProduct->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Image>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduct($this);
        }

        return $this;
    }

    public function removeImage(Image $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getProduct() === $this) {
                $image->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setProduct($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getProduct() === $this) {
                $comment->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Grade>
     */
    public function getGrades(): Collection
    {
        return $this->grades;
    }

    public function addGrade(Grade $grade): self
    {
        if (!$this->grades->contains($grade)) {
            $this->grades[] = $grade;
            $grade->setProduct($this);
        }

        return $this;
    }

    public function removeGrade(Grade $grade): self
    {
        if ($this->grades->removeElement($grade)) {
            // set the owning side to null (unless already changed)
            if ($grade->getProduct() === $this) {
                $grade->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Imagick>
     */
    public function getImagicks(): Collection
    {
        return $this->imagicks;
    }

    public function addImagick(Imagick $imagick): self
    {
        if (!$this->imagicks->contains($imagick)) {
            $this->imagicks[] = $imagick;
            $imagick->setProduct($this);
        }

        return $this;
    }

    public function removeImagick(Imagick $imagick): self
    {
        if ($this->imagicks->removeElement($imagick)) {
            // set the owning side to null (unless already changed)
            if ($imagick->getProduct() === $this) {
                $imagick->setProduct(null);
            }
        }

        return $this;
    }

}
