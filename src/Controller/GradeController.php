<?php

namespace App\Controller;

use App\Entity\Grade;
use App\Repository\GradeRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class GradeController extends AbstractController
{
    public Security $security;
    private ProductRepository $productRepository;
    private EntityManagerInterface $entityManager;
    private GradeRepository $gradeRepository;

    /**
     * @param Security $security
     */
    public function __construct
    (
        Security               $security,
        ProductRepository      $productRepository,
        EntityManagerInterface $entityManager,
        GradeRepository        $gradeRepository
    )
    {
        $this->gradeRepository = $gradeRepository;
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->security = $security;
    }

    #[Route('/grade/store', name: 'app_grade_store', methods: ['POST'])]
    public function store(Request $request): Response
    {
        $product = $this->productRepository->find($request->get('product_id'));
        $user = $this->security->getUser();

        $is_grated = $this->gradeRepository->findBy(['user' => $user->getId(), 'product' => $product->getId()]);

        if (count($is_grated) == 0 && $request->get('options') !== null) {
            $grade = new Grade();
            $grade->setUser($user);
            $grade->setProduct($product);
            $grade->setGrade($request->get('options'));
            $this->entityManager->persist($grade);
            $this->entityManager->flush();
        }

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/grade/destroy/{id}', name: 'app_grade_destroy', methods: ['POST'])]
    public function destroy(Request $request, Grade $grade): Response
    {
        $this->gradeRepository->remove($grade);
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}
