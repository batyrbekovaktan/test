<?php

namespace App\Controller;

use App\Entity\Brand;
use App\Form\BrandType;
use App\Repository\BrandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/brand')]
class BrandController extends AbstractController
{
    /**
     * @param BrandRepository $brandRepository
     * @return Response
     */
    #[Route('/', name: 'app_brand_index', methods: ['GET'])]
    public function index(BrandRepository $brandRepository): Response
    {

//        $url = "https://hoff.ru/vue/catalog/section/?category_id=687&limit=30&offset=0&showCount=1&type=product_list";
//
//        $client = HttpClient::create();
////        $response = $client->request('GET', $url);
////        $pieces = explode(";", $response->getHeaders()['set-cookie'][0]);
//
//        $client = $client->withOptions([
//            'headers' => [
//                'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
//                'cookie' => 'PHPSESSID=60om6rv4k7un72s4c3e5778m32; iwaf_http_cookie_291e829ea12795e7e56f937263071616=c9af2bb9932b5c1249d56f4275ca75e8; iwaf_js_cookie_291e829ea12795e7e56f937263071616=f8d179143d623d0cadc4ca7411cf6b77'
//                 ]
//        ]);
//        $response = $client->request('GET', $url);
//        $data = json_decode($response->getContent(), true);
//        dd($data);

        return $this->render('brand/index.html.twig', [
            'brands' => $brandRepository->findAll(),
        ]);
    }

    /**
     * @param Request $request
     * @param BrandRepository $brandRepository
     * @return Response
     */
    #[Route('/new', name: 'app_brand_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BrandRepository $brandRepository): Response
    {
        $brand = new Brand();
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $brandRepository->add($brand);
            return $this->redirectToRoute('app_brand_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('brand/new.html.twig', [
            'brand' => $brand,
            'form' => $form,
        ]);
    }

    /**
     * @param Brand $brand
     * @return Response
     */
    #[Route('/{id}', name: 'app_brand_show', methods: ['GET'])]
    public function show(Brand $brand): Response
    {
        return $this->render('brand/show.html.twig', [
            'brand' => $brand,
        ]);
    }

    /**
     * @param Request $request
     * @param Brand $brand
     * @param BrandRepository $brandRepository
     * @return Response
     */
    #[Route('/{id}/edit', name: 'app_brand_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Brand $brand, BrandRepository $brandRepository): Response
    {
        $form = $this->createForm(BrandType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $brandRepository->add($brand);
            return $this->redirectToRoute('app_brand_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('brand/edit.html.twig', [
            'brand' => $brand,
            'form' => $form,
        ]);
    }

    /**
     * @param Request $request
     * @param Brand $brand
     * @param BrandRepository $brandRepository
     * @return Response
     */
    #[Route('/{id}', name: 'app_brand_delete', methods: ['POST'])]
    public function delete(Request $request, Brand $brand, BrandRepository $brandRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $brand->getId(), $request->request->get('_token'))) {
            $brandRepository->remove($brand);
        }

        return $this->redirectToRoute('app_brand_index', [], Response::HTTP_SEE_OTHER);
    }
}
