<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Repository\CurrencyRepository;
use App\Repository\ProductRepository;
use App\Services\CartAll;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Dompdf\Dompdf;
use Dompdf\Options;

#[Route('/order')]
class OrderController extends AbstractController
{
    public Session $session;
    public Security $security;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
        $this->session = new Session();
    }

    /**
     * @return Response
     */
    #[Route('/', name: 'order_index', methods: ['GET'])]
    public function index(): Response
    {
        $user = $this->security->getUser();
        $orders = $user->getOrders();
        return $this->render('order/index.html.twig', [
            'orders' => $orders,
        ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     * @param CurrencyRepository $currencyRepository
     * @return Response
     */
    #[Route('/new', name: 'order_store', methods: ['GET', 'POST'])]
    public function store(EntityManagerInterface $entityManager, ProductRepository $productRepository, CurrencyRepository $currencyRepository): Response
    {
        $total = new CartAll($currencyRepository);
        $user = $this->security->getUser();
        $order = new Order();
        $order->setUser($user);
        $order->setTotal($total->getTotal());
        $order->setCreatedAt(new DateTimeImmutable());
        $entityManager->persist($order);

        foreach ($this->session->get('products') as $item) {
            $orderItem = new OrderProduct();
            $product = $productRepository->find($item['id']);
            $orderItem->setProduct($product);
            $orderItem->setQty($item['qty']);
            $orderItem->setOrder($order);
            $entityManager->persist($orderItem);
        }
        $entityManager->flush();

        $this->session->remove('products');

        return $this->redirectToRoute('product_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/create', name: 'order_create', methods: ['GET'])]
    public function create(EntityManagerInterface $entityManager): Response
    {
        $products = $entityManager->createQuery('SELECT c FROM App\Entity\Product c')->setMaxResults(5)->getResult();
        return $this->render('order/create.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @param Order $order
     * @return Response
     */
    #[Route('/{id}', name: 'order_show', methods: ['GET'])]
    public function show(Order $order): Response
    {
        return $this->render('order/show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse|RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    #[Route('/order/export', name: 'user_order_download_excel')]
    public function export(Request $request): BinaryFileResponse|RedirectResponse
    {
        if ($this->security->getUser()->getOrders()->count() > 0) {
            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setTitle('Order List');

            $sheet->getCell('A1')->setValue('Order');
            $sheet->getCell('B1')->setValue('Total');
            $sheet->getCell('C1')->setValue('Created At');

            $list = [];
            foreach ($this->security->getUser()->getOrders() as $order) {
                    $list[] = [
                        $order->getId(),
                        $order->getTotal() . '$',
                        $order->getCreatedAt()->format('Y-m-d')
                    ];
                    $list[] = ['', '', '', 'Product', 'qty', 'Price', 'Total'];
                    foreach ($order->getOrderProducts() as $product) {
                        $list[] = [
                            '',
                            '',
                            '',
                            $product->getProduct()->getName(),
                            $product->getQty(),
                            $product->getProduct()->getPrice() . '$',
                           ($product->getProduct()->getPrice() * $product->getQty()) . '$',
                        ];
                    }
            }
            $sheet->fromArray($list, null, 'A2', true);

            $writer = new Xlsx($spreadsheet);

            $writer->save('user_order.xlsx');
            return $this->file($this->getParameter('kernel.project_dir') . '/public/user_order.xlsx', 'user_order.xlsx');
        } else {
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }
    }


    /**
     * @param Request $request
     * @return RedirectResponse|void
     */
    #[Route('/order/export/pdf', name: 'user_order_download_pdf')]
    public function exportPdf(Request $request)
    {
        if ($this->security->getUser()->getOrders()->count() > 0) {

            $user = $this->security->getUser();
            $orders = $user->getOrders();

            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $pdfOptions->set('isRemoteEnabled', TRUE);
            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView('order/order.html.twig', [
                'orders' => $orders,
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();

            $dompdf->stream("My_order.pdf", [
                "Attachment" => true
            ]);

        } else {
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }
    }

}
