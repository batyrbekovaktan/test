<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use DateTime;

#[Route('/comment')]
class CommentController extends AbstractController
{
    public Security $security;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/new', name: 'app_comment_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CommentRepository $commentRepository, ProductRepository $productRepository, ValidatorInterface $validator): Response
    {
        $product = $productRepository->find($request->get('product_id'));
        $comment = new Comment();
        $comment->setContent($request->get('content'));
        $comment->setProduct($product);
        $comment->setCreatedAt(new \DateTimeImmutable());
        $comment->setUser($this->security->getUser());
        $errors = $validator->validate($comment);

        if (count($errors) > 0) {
            $errorsString = (string)$errors;
//            dd($errors);
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        $commentRepository->add($comment);
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/{id}', name: 'app_comment_delete', methods: ['POST'])]
    public function delete(Request $request, Comment $comment, CommentRepository $commentRepository): Response
    {
        $commentRepository->remove($comment);
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}
