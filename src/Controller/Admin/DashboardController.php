<?php

namespace App\Controller\Admin;

use App\Entity\Brand;
use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Product;
use App\Entity\SliderImage;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return parent::index();
    }

    /**
     * @return Dashboard
     */
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('My Test Project');
    }

    /**
     * @return iterable
     */
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Back to the website', 'fas fa-home', 'product_index');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Users', 'fas fa-map-marker-alt', User::class);
        yield MenuItem::linkToCrud('Products', 'fas fa-comments', Product::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-comments', Category::class);
        yield MenuItem::linkToCrud('SliderImages', 'fas fa-comments', SliderImage::class);
        yield MenuItem::linkToCrud('Post', 'fas fa-comments', Post::class);
        yield MenuItem::linkToCrud('Brand', 'fas fa-comments', Brand::class);
        yield MenuItem::linktoRoute('Excel', 'fas fa-map-marker-alt', 'excel_create');
        yield MenuItem::linktoRoute('Jobs', 'fas fa-map-marker-alt', 'job_index');
        yield MenuItem::linktoRoute('Images', 'fas fa-map-marker-alt', 'app_image');
        yield MenuItem::linktoRoute('Proxy', 'fas fa-map-marker-alt', 'proxy_create');
    }
}
