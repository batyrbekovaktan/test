<?php

namespace App\Controller\Admin;

use App\Entity\Job;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class JobController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/admin/job', name: 'job_index')]
    public function index(): Response
    {
        return $this->renderForm('job/index.html.twig', []);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return RedirectResponse
     */
    #[Route('/admin/job/store', name: 'job_store')]
    public function store(EntityManagerInterface $entityManager, Request $request): RedirectResponse
    {
        $job = $entityManager->getRepository(Job::class)->findOneBy([]);

        if ($job->getStatus() == 'in progress') {
            $referer = $request->headers->get('referer');
            return $this->redirect($referer);
        }

        if ($job->getStatus() == 'done') {
            $job->setStatus('to start');
            $entityManager->persist($job);
            $entityManager->flush();
        }

        $process = new Process(['php', $this->getParameter('kernel.project_dir') . '/bin/console', 'GetProducts']);
        $process->setOptions(['create_new_console' => true]);
        $process->start();

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    #[Route('/admin/job/status', name: 'job_status')]
    public function getStatus(EntityManagerInterface $entityManager): JsonResponse
    {
        $job = $entityManager->getRepository(Job::class)->findOneBy([]);

        return $this->json(['qty' => $job->getQty(), 'status' => $job->getStatus()], 200);
    }
}
