<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\User;
use App\Repository\OrderRepository;
use DateTimeImmutable;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/admin/excel/create', name: 'excel_create', methods: ["GET"])]
    public function create(): Response
    {
        return $this->renderForm('excel/index.html.twig', []);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    #[Route('/admin/excel/store', name: 'excel_store')]
    public function store(Request $request): Response
    {
        $file = $request->files->get('file');
        $fileFolder = $this->getParameter('kernel.project_dir') . '/public/uploads/';

        $filePathName = md5(uniqid()) . $file->getClientOriginalName();
        $file->move($fileFolder, $filePathName);

        $spreadsheet = IOFactory::load($fileFolder . $filePathName);
        $row = $spreadsheet->getActiveSheet()->removeRow(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $entityManager = $this->getDoctrine()->getManager();
        foreach ($sheetData as $Row) {
            $name = $Row['A'];
            $total = $Row['B'];

            $user_exist = $entityManager->getRepository(User::class)->findOneBy(array('name' => $name));

            if ($user_exist) {
                $order = new Order();
                $order->setUser($user_exist);
                $order->setTotal($total);
                $order->setCreatedAt(new DateTimeImmutable());
                $entityManager->persist($order);
                $entityManager->flush();
            }
        }

        unlink($this->getParameter('kernel.project_dir') . '/public/uploads/' . $filePathName);


        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws Exception
     */
    #[Route('/admin/export', name: 'export')]
    public function export(OrderRepository $orderRepository): BinaryFileResponse
    {
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setTitle('Order List');

        $sheet->getCell('A1')->setValue('Full Name');
        $sheet->getCell('B1')->setValue('Total');
        $sheet->getCell('C1')->setValue('Created At');

        $list = [];
        $orders = $orderRepository->findAll();
        foreach ($orders as $order) {
            if ($order->getUser()) {
                $list[] = [
                    $order->getUser()->getName(),
                    $order->getTotal(),
                    $order->getCreatedAt()
                ];
            }
        }
        $sheet->fromArray($list, null, 'A2', true);

        $writer = new Xlsx($spreadsheet);


        $writer->save('orders.xlsx');
        return $this->file($this->getParameter('kernel.project_dir') . '/public/orders.xlsx', 'Orders.xlsx');
    }

}
