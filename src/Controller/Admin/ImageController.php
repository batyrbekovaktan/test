<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use App\Entity\Mini;
use App\ImageOptimizer;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Imagick;
use ImagickException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends AbstractController
{
    private ImageOptimizer $imageOptimizer;
    private EntityManagerInterface $_em;
    private ProductRepository $productRepository;
    private int $big_size = 200;

    /**
     * @param ImageOptimizer $imageOptimizer
     * @param EntityManagerInterface $entityManager
     * @param ProductRepository $productRepository
     */
    public function __construct(ImageOptimizer $imageOptimizer, EntityManagerInterface $entityManager, ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
        $this->_em = $entityManager;
        $this->imageOptimizer = $imageOptimizer;
    }

    /**
     * @param ProductRepository $productRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/admin/image', name: 'app_image', methods: ['GET'])]
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $all_products = $this->productRepository->createQueryBuilder('p')->getQuery();

        $products = $paginator->paginate(
            $all_products,
            $request->query->getInt('page', 1),
            7
        );

        return $this->render('image/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @throws ORMException
     * @throws ImagickException
     */
    #[Route('/admin/image', name: 'new_image', methods: ['POST'])]
    public function store(Request $request): Response
    {
        $product = $this->productRepository->findOneBy(['id' => $request->get('product_id')]);


        if ($request->files->get('pictures')) {
            foreach ($request->files->get('pictures') as $file) {

                if ($file) {

                    $width = getimagesize($file)[0];
                    $height = getimagesize($file)[1];

                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                    $newFilename = md5($originalFilename) . '.' . $file->guessExtension();

                    $format = preg_replace('/^.*\.(.*)$/U', '$1', $newFilename);

                    $file->move(
                        $this->getParameter('pictures_directory') . '/products/',
                        $newFilename
                    );

                    $imagick = new Imagick($this->getParameter('pictures_directory') . '/products/' . $newFilename);

                    $imagick->setCompression(Imagick::COMPRESSION_JPEG);
                    $imagick->adaptiveResizeImage($width, $height);
                    $imagick->setImageCompressionQuality(75);
                    $imagick->stripImage();

                    unlink($this->getParameter('pictures_directory') . '/products/' . $newFilename);

                    $imagick->writeImages($this->getParameter('pictures_directory') . '/products/' . $newFilename, true);

                    $image = new Image();
                    $image->setImage($newFilename);
                    $image->setProduct($product);
                    $this->_em->persist($image);

                    $this->imageOptimizer
                        ->resize(
                            $this->getParameter('pictures_directory') . '/products/' . $newFilename,
                            $this->getParameter('pictures_directory') . '/resize/' . $newFilename);

                    $mini = new Mini();
                    $mini->setPicture($newFilename);
                    $mini->setImage($image);
                    $mini->setType($format);
                    $this->_em->persist($mini);

                }

            }
            $this->_em->flush();
        }
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}
