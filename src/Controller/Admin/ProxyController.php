<?php

namespace App\Controller\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProxyController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route('/admin/proxy/store', name: 'proxy_create', methods: ["GET"])]
    public function index(): Response
    {
        return $this->renderForm('proxy/index.html.twig', []);
    }
}
