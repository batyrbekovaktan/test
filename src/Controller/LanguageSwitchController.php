<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LanguageSwitchController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     */
    #[Route('/language/switch', name: 'app_language_switch', methods: ['POST'])]
    public function switch(Request $request): Response
    {
        $request->getSession()->set('_locale', $request->get('_locale'));
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }
}
