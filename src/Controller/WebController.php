<?php

namespace App\Controller;

use App\Entity\Proxy;
use App\Repository\ProxyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class WebController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ProxyRepository $proxyRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ProxyRepository $proxyRepository
     */
    public function __construct(EntityManagerInterface $entityManager, ProxyRepository $proxyRepository)
    {
        $this->proxyRepository = $proxyRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/web', name: 'app_web', methods: ["GET"])]
    public function index(): JsonResponse
    {
        $url = "https://spys.one/proxys/RU/";

        $client = HttpClient::create();

        $response = $client->request('GET', $url);

        $crawler = new Crawler($response->getContent());

        $array = $crawler->filter('.spy14')->each(function ($node) {
            $name = $node->text();
            return compact('name');
        });

        $evals = $crawler->filter('html')->each(function ($node) {
            $name = $node->filterXPath('//text()[contains(.,"eval(function(p,r,o,x,y,s)")]')->text();
            return compact('name');
        });

        return $this->json(['data' => $this->getData($array), 'eval' => $evals[0]['name']], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/web/store/', name: 'app_web_store', methods: ["POST"])]
    public function store(Request $request): JsonResponse
    {
        foreach ($request->get('array') as $code) {
            $exist = $this->proxyRepository->findOneBy(['proxy' => $code]);
            if (!$exist) {
                $proxy = new Proxy();
                $proxy->setProxy($code);
                $this->entityManager->persist($proxy);
            }
        }

        $this->entityManager->flush();

        return $this->json(['data' => 'yes']);
    }

    /**
     * @param $array
     * @return array
     */
    private function getData($array): array
    {
        $data = [];

        for ($i = 0; $i < 115; $i++) {
            $str = strstr($array[$i]['name'], '+(');
            $codes = explode("+", $str);
            array_shift($codes);
            if ($codes) {
                $letter = strpos($array[$i]['name'], 'd');
                $proxy = substr($array[$i]['name'], 0, $letter);
                $new_codes = [];
                foreach ($codes as $code) {
                    $new_codes[] = str_replace([')', '('], '', $code);
                }
                $data[] = ['proxy' => $proxy, 'codes' => $new_codes];
            }
        }

        return $data;
    }
}
