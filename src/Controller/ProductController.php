<?php

namespace App\Controller;

use App\Entity\Email;
use App\Entity\Grade;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Services\Search;
use Doctrine\ORM\EntityManagerInterface;
use IntlChar;
use Phpml\Classification\KNearestNeighbors;
use Phpml\Regression\LeastSquares;
use thiagoalessio\TesseractOCR\TesseractOCR;
use Elasticsearch\ClientBuilder;
use Imagick;
use ImagickException;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\ElasticaBundle\Finder\PaginatedFinderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ProductController extends AbstractController
{
    private PaginatedFinderInterface $finder;
    private EntityManagerInterface $entityManager;
    private Session $session;
    private Security $security;
    private ProductRepository $productRepository;

    /**
     * @param PaginatedFinderInterface $finder
     * @param EntityManagerInterface $entityManager
     * @param Security $security
     * @param ProductRepository $productRepository
     */
    public function __construct(
        PaginatedFinderInterface $finder,
        EntityManagerInterface   $entityManager,
        Security                 $security,
        ProductRepository        $productRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->security = $security;
        $this->session = new Session();
        $this->entityManager = $entityManager;
        $this->finder = $finder;
    }

    /**
     * @Route("/all/products", name="product_all", methods={"GET", "POST"})
     */
    public function products(PaginatorInterface $paginator, Request $request): Response
    {

        $all_products = $this->productRepository->createQueryBuilder('p')->getQuery();

        $products = $paginator->paginate(
            $all_products,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('product/products.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/products", name="product_index", methods={"GET", "POST"})
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $all_products = $this->productRepository->createQueryBuilder('p')->getQuery();

        $products = $paginator->paginate(
            $all_products,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products
        ]);
    }

    /**
     * @Route("/products/search", name="product_search", methods={"POST", "GET"})
     * @throws \Exception
     */
    public function search(Request $request, PaginatorInterface $paginator): Response
    {
        if ($request->get('name')) {
            $this->session->set('search', $request->get('name'));
        }

        $words = $this->session->get('search');

        $name = Search::search($words);

        try {
            $new_products = $this->finder->find($name, 100);
        } catch (\Exception $e) {
            $new_products = $this->productRepository->findBy(['name' => $name]);
        }

        $products = $paginator->paginate(
            $new_products,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('product/search.html.twig', [
            'products' => $products
        ]);


//        $translitRu = $this->translitRu($words);
//        $translit = $this->translit($words);
//        $switcher = $this->switcher_en($words);
//        $ruSwitcher = $this->switcher_ru($words);

//        try {
//            $new_products = $this->finder->find($translitRu . $translit . $words . $switcher . $ruSwitcher, 100);
//        } catch (\Exception $e) {
//        $new_products = $this->productRepository->findBy(['name' => $request->get('name')]);
//        }

    }

    /**
     * @Route("/products/top", name="product_top", methods={"GET"})
     */
    public function top(ProductRepository $productRepository): Response
    {
        $products = $productRepository->orderByPrice(6);

        return $this->render('product/top.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/{id}", name="product_show", methods={"GET"})
     */
    public function show(int $id, ProductRepository $productRepository, EntityManagerInterface $entityManager): Response
    {
        $product = $productRepository->find($id);

        $total = 0;
        foreach ($product->getGrades() as $grade) {
            $total += $grade->getGrade();
        }
        if ($total != 0) {
            $total = ($total / $product->getGrades()->count());
        }

        $user = $this->security->getUser();
        $is_grated = $entityManager->getRepository(Grade::class)->findOneBy(['user' => $user->getId(), 'product' => $product->getId()]);

        $products = $entityManager->createQuery('SELECT c FROM App\Entity\Product c')->setMaxResults(5)->getResult();
        return $this->render('product/show.html.twig', [
            'products' => $products,
            'product' => $product,
            'total' => $total,
            'is_grated' => $is_grated
        ]);
    }


    /**
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @return RedirectResponse
     * @throws ImagickException
     * @throws \thiagoalessio\TesseractOCR\TesseractOcrException
     */
    #[Route('/product/new', name: 'product_new', methods: ['POST'])]
    public function new(Request $request, CategoryRepository $categoryRepository): RedirectResponse
    {
//        $picture = $request->files->get('file');
//        if ($picture) {
//            $originalFilename = pathinfo($picture->getClientOriginalName(), PATHINFO_FILENAME);
//
//            $newFilename = $originalFilename . '-' . uniqid() . '.' . $picture->guessExtension();
//
//            $picture->move(
//                $this->getParameter('pictures_directory') . '/products/',
//                $newFilename
//            );
//
//            $ocr = new TesseractOCR();
//            foreach ($ocr->availableLanguages() as $lang) {
//                dump($lang);
//            }
//            $data = (new TesseractOCR($this->getParameter('pictures_directory') . '/products/' . $newFilename))
//                ->run();
//
//            $data = str_replace("\n", " ", $data);
//
//            $email = new Email();
//            $email->setEmail($data);
//            $this->entityManager->persist($email);
//
//            dd($email->getEmail());
//        }



//        $category = $categoryRepository->find($request->get('category_id'));
//            $product = new Product();
//            $product->setName($request->get('name'));
//            $product->setPrice($request->get('price'));
//            $product->setCategory($category);
//            $product->setPicture($newFilename);
//            $product->setDescription($request->get('description'));
//            $this->entityManager->persist($product);
//
//            $this->entityManager->flush();
//            $client = ClientBuilder::create()->build();
//
//            $params = [
//                "index" => "data.db",
//                "type" => "product",
//                "body" => [
//                    "name" => $product->getName(),
//                    "price" => $product->getPrice(),
//                    "category" => $product->getCategory(),
//                ]
//            ];
//            $response = $client->index($params);




        $referer = $request->headers->get('referer');
        $this->addFlash('success', 'Success');
        $this->addFlash('danger', 'Error');
        return $this->redirect($referer);
    }


    /**
     * @param Request $request
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    #[Route('/products/create', name: 'product_create', methods: ['GET'])]
    public function create(Request $request, CategoryRepository $categoryRepository): Response
    {
        return $this->render('product/create.html.twig', [
            'categories' => $categoryRepository->findAll()
        ]);
    }

    /**
     * @param $value
     * @return string
     */
    public function translit($value): string
    {
        $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
            'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
            'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D',
            'Е' => 'E', 'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
            'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'Ch',
            'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
        );

        $value = strtr($value, $converter);
        return $value;
    }


    /**
     * @param $input
     * @return string
     */
    public function translitRu($input): string
    {
        $gost = array(
            "a" => "а", "b" => "б", "v" => "в", "g" => "г", "d" => "д", "yo" => "ё",
            "j" => "ж", "z" => "з", "i" => "и", "i" => "й", "k" => "к",
            "l" => "л", "m" => "м", "n" => "н", "o" => "о", "p" => "п", "r" => "р", "s" => "с", "t" => "т",
            "y" => "у", "f" => "ф", "h" => "х", "c" => "ц",
            "ch" => "ч", "sh" => "ш", "sh" => "щ", "i" => "ы", "e" => "е", "u" => "у", "ya" => "я", "A" => "А", "B" => "Б",
            "V" => "В", "G" => "Г", "D" => "Д", "E" => "Е", "Yo" => "Ё", "J" => "Ж", "Z" => "З", "I" => "И", "I" => "Й", "K" => "К", "L" => "Л", "M" => "М",
            "N" => "Н", "O" => "О", "P" => "П",
            "R" => "Р", "S" => "С", "T" => "Т", "Y" => "Ю", "F" => "Ф", "H" => "Х", "C" => "Ц", "Ch" => "Ч", "Sh" => "Ш",
            "Sh" => "Щ", "I" => "Ы", "E" => "Е", "U" => "У", "Ya" => "Я", "'" => "ь", "'" => "Ь", "''" => "ъ", "''" => "Ъ", "j" => "ї", "i" => "и",
            "ye" => "є", "J" => "Ї", "I" => "І",
            "YE" => "Є"
        );
        return strtr($input, $gost);
    }

    /**
     * @param $value
     * @return string
     */
    public function switcher_en($value): string
    {
        $converter = array(
            'а' => 'f', 'б' => ',', 'в' => 'd', 'г' => 'u', 'д' => 'l', 'е' => 't', 'ё' => '`',
            'ж' => ';', 'з' => 'p', 'и' => 'b', 'й' => 'q', 'к' => 'r', 'л' => 'k', 'м' => 'v',
            'н' => 'y', 'о' => 'j', 'п' => 'g', 'р' => 'h', 'с' => 'c', 'т' => 'n', 'у' => 'e',
            'ф' => 'a', 'х' => '[', 'ц' => 'w', 'ч' => 'x', 'ш' => 'i', 'щ' => 'o', 'ь' => 'm',
            'ы' => 's', 'ъ' => ']', 'э' => "'", 'ю' => '.', 'я' => 'z',

            'А' => 'F', 'Б' => '<', 'В' => 'D', 'Г' => 'U', 'Д' => 'L', 'Е' => 'T', 'Ё' => '~',
            'Ж' => ':', 'З' => 'P', 'И' => 'B', 'Й' => 'Q', 'К' => 'R', 'Л' => 'K', 'М' => 'V',
            'Н' => 'Y', 'О' => 'J', 'П' => 'G', 'Р' => 'H', 'С' => 'C', 'Т' => 'N', 'У' => 'E',
            'Ф' => 'A', 'Х' => '{', 'Ц' => 'W', 'Ч' => 'X', 'Ш' => 'I', 'Щ' => 'O', 'Ь' => 'M',
            'Ы' => 'S', 'Ъ' => '}', 'Э' => '"', 'Ю' => '>', 'Я' => 'Z',

            '"' => '@', '№' => '#', ';' => '$', ':' => '^', '?' => '&', '.' => '/', ',' => '?',
        );

        $value = strtr($value, $converter);
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function switcher_ru($value): string
    {
        $converter = array(
            'f' => 'а', ',' => 'б', 'd' => 'в', 'u' => 'г', 'l' => 'д', 't' => 'е', '`' => 'ё',
            ';' => 'ж', 'p' => 'з', 'b' => 'и', 'q' => 'й', 'r' => 'к', 'k' => 'л', 'v' => 'м',
            'y' => 'н', 'j' => 'о', 'g' => 'п', 'h' => 'р', 'c' => 'с', 'n' => 'т', 'e' => 'у',
            'a' => 'ф', '[' => 'х', 'w' => 'ц', 'x' => 'ч', 'i' => 'ш', 'o' => 'щ', 'm' => 'ь',
            's' => 'ы', ']' => 'ъ', "'" => "э", '.' => 'ю', 'z' => 'я',

            'F' => 'А', '<' => 'Б', 'D' => 'В', 'U' => 'Г', 'L' => 'Д', 'T' => 'Е', '~' => 'Ё',
            ':' => 'Ж', 'P' => 'З', 'B' => 'И', 'Q' => 'Й', 'R' => 'К', 'K' => 'Л', 'V' => 'М',
            'Y' => 'Н', 'J' => 'О', 'G' => 'П', 'H' => 'Р', 'C' => 'С', 'N' => 'Т', 'E' => 'У',
            'A' => 'Ф', '{' => 'Х', 'W' => 'Ц', 'X' => 'Ч', 'I' => 'Ш', 'O' => 'Щ', 'M' => 'Ь',
            'S' => 'Ы', '}' => 'Ъ', '"' => 'Э', '>' => 'Ю', 'Z' => 'Я',

            '@' => '"', '#' => '№', '$' => ';', '^' => ':', '&' => '?', '/' => '.', '?' => ',',
        );

        $value = strtr($value, $converter);
        return $value;
    }
}
