<?php

namespace App\Controller;

use App\Repository\CurrencyRepository;
use App\Repository\ProductRepository;
use App\Services\CartAll;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class CartController extends AbstractController
{
    public Session $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return Response
     */
    #[Route('/cart/store', name: 'cart_store', methods: ['POST', 'GET'])]
    public function store(Request $request, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($request->get('product_id'));
        $session = $this->session->get('products');
        if (isset($session[$product->getId()])) {
            $session[$product->getId()]['qty'] = $session[$product->getId()]['qty'] + 1;
        } else {
            $session[$product->getId()] = ['name' => $product->getName(),'qty' => 1, 'price' => $product->getPrice(), 'id' => $product->getId()];
        }
        $this->session->set('products', $session);

        return $this->render('cart/product.html.twig', [
            'product' => $session[$product->getId()]
        ]);
    }

    /**
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return Response
     */
    #[Route('/cart/update', name: 'cart_update', methods: ['GET', 'POST'])]
    public function update(Request $request, ProductRepository $productRepository): Response
    {
        $product = $productRepository->find($request->get('product_id'));
        $session = $this->session->get('products');
        switch ($request->get('type')) {
            case 'minus':
                if ($session[$product->getId()]['qty'] > 1) {
                    $session[$product->getId()]['qty'] = $session[$product->getId()]['qty'] - 1;
                }
                break;
            case 'plus':
                $session[$product->getId()]['qty'] = $session[$product->getId()]['qty'] + 1;
                break;
        }
        $this->session->set('products', $session);

        return $this->render('cart/product.html.twig', [
            'product' => $session[$product->getId()]
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    #[Route('/cart/delete', name: 'cart_delete', methods: ['POST'])]
    public function destroy(Request $request): JsonResponse
    {
        $session = $this->session->get('products');
        unset($session[$request->get('product_id')]);
        $this->session->set('products', $session);
        return $this->json([], 204);
    }

    /**
     * @param CurrencyRepository $currencyRepository
     * @return JsonResponse
     */
    #[Route('/cart/total', name: 'cart_total', methods: ['GET'])]
    public function total(CurrencyRepository $currencyRepository): JsonResponse
    {
        $cart = new CartAll($currencyRepository);
        return $this->json(['total' => $cart->getTotal()], 200);
    }
}
