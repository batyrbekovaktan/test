<?php

namespace App\Controller;

use App\Repository\CurrencyRepository;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PaymentController extends AbstractController
{
    public \Symfony\Component\HttpFoundation\Session\Session $session;
    private CurrencyRepository $currencyRepository;


    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
        $this->session = new \Symfony\Component\HttpFoundation\Session\Session();
    }

    /**
     * @return Response
     */
    #[Route('/payment', name: 'payment')]
    public function index(): Response
    {
        return $this->render('payment/index.html.twig', [
            'controller_name' => 'PaymentController',
        ]);
    }

    /**
     * @param $stripeSK
     * @return RedirectResponse
     * @throws ApiErrorException
     */
    #[Route('/checkout', name: 'checkout')]
    public function checkout($stripeSK): RedirectResponse
    {

        $total = 0;

        if ($this->session->get('products')) {
            foreach ($this->session->get('products') as $product) {
                $total += $product['price'] * $product['qty'];
            }
        }

        $currency = $this->currencyRepository->findOneBy(['code' => 'USD'])->getValue();

        $total = ceil($total / $currency);
        Stripe::setApiKey($stripeSK);
        $session = Session::create([
            'line_items' => [[
                'price_data' => [
                    'currency' => 'usd',
                    'product_data' => [
                        'name' => 'Products',
                    ],
                    'unit_amount' => $total . '00',
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('order_store', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('cansel_url', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        return $this->redirect($session->url, 303);
    }

    /**
     * @return Response
     */
    #[Route('/cansel_url', name: 'cansel_url')]
    public function canselUrl(): Response
    {
        return $this->render('payment/cansel.html.twig', []);
    }
}
