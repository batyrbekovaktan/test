<?php

namespace App\Controller;

use App\Entity\Email;
use App\Repository\EmailRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/email')]
class EmailController extends AbstractController
{
    /**
     * @param EmailRepository $emailRepository
     * @return Response
     */
    #[Route('/', name: 'email_index', methods: ['GET'])]
    public function index(EmailRepository $emailRepository): Response
    {
        return $this->render('email/index.html.twig', [
            'emails' => $emailRepository->findAll(),
        ]);
    }

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route('/new', name: 'email_new', methods: ['POST'])]
    public function new(ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $entityManager = $doctrine->getManager();
        $product = new Email();

        $product->setEmail($request->get('email'));

        $entityManager->persist($product);
        $entityManager->flush();

        return $this->redirectToRoute('email_index');
    }


}
