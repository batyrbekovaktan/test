<?php

namespace App\Command;

use App\Entity\Image;
use App\Entity\Mini;
use App\Entity\Product;
use App\ImageOptimizer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Validator\ValidatorInterface;


#[AsCommand(
    name: 'NewProducts',
    description: 'Get new products',
)]
class NewProductsCommand extends Command
{
    public EntityManagerInterface $entityManager;
    public ContainerBagInterface $params;
    public ValidatorInterface $validator;
    private ImageOptimizer $imageOptimizer;

    /**
     * @param ImageOptimizer $imageOptimizer
     * @param EntityManagerInterface $entityManager
     * @param ContainerBagInterface $params
     * @param ValidatorInterface $validator
     */
    public function __construct(ImageOptimizer $imageOptimizer, EntityManagerInterface $entityManager, ContainerBagInterface $params, ValidatorInterface $validator)
    {
        $this->imageOptimizer = $imageOptimizer;
        $this->params = $params;
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $categories = ['687', '633', '3443', '1022'];

        foreach ($categories as $category) {

            $url = 'https://hoff.ru/vue/catalog/section/?category_id=' . $category . '&limit=30&offset=0&showCount=1&type=product_list';

            $client = HttpClient::create();

            $client = $client->withOptions([
                'headers' => [
                    'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
                    'cookie' => 'PHPSESSID=60om6rv4k7un72s4c3e5778m32; iwaf_http_cookie_291e829ea12795e7e56f937263071616=7d7f408cfae0122cb446473898511372; iwaf_js_cookie_291e829ea12795e7e56f937263071616=dcaac503ff26dc59973a8bb8a70b7765'
                ]
            ]);
            $response = $client->request('GET', $url);
            $data = json_decode($response->getContent(), true);

            $offset = 0;

            for ($i = 1; $i < $data['data']['pagination']['total']; $i++) {
                $url = 'https://hoff.ru/vue/catalog/section/?category_id=' . $category . '&limit=60&offset=" . $offset . "&showCount=1&type=product_list';
                $response = $client->request('GET', $url);
                $data = json_decode($response->getContent(), true);
                $offset += 30;


                foreach ($data['data']['items'] as $product) {
                    if (isset($product['name'])) {
                        $new_product = new Product();
                        $new_product->setName($product['name']);
                        $new_product->setDescription('product from hoff.ru');
                        $new_product->setPrice($product['prices']['new']);

                        $errors = $this->validator->validate($new_product);

                        if (count($errors) == 0) {
                            if (isset($product['image'])) {
                                $picture = $this->getImage($product['image']);
                                $new_product->setPicture($picture);

                                foreach ($product['images'] as $image) {
                                    $new_picture = $this->getImage($image);
                                    $new_image = new Image();
                                    $new_image->setImage($new_picture);
                                    $new_image->setProduct($new_product);
                                    $this->entityManager->persist($new_image);
                                    $this->imageOptimizer
                                        ->resize(
                                            $this->params->get('pictures_directory') . '/products/' . $new_picture,
                                            $this->params->get('pictures_directory') . '/resize/' . $new_picture,
                                        );
                                    $mini = new Mini();
                                    $mini->setPicture($new_picture);
                                    $mini->setImage($new_image);
                                    $mini->setType(preg_replace("/.*?\./", '', $new_picture));
                                    $this->entityManager->persist($mini);
                                }
                            }
                            $this->entityManager->persist($new_product);
                        }
                    }
                    $this->entityManager->flush();
                }
            }
        }
        return Command::SUCCESS;
    }


    /**
     * @param $product
     * @return string
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getImage($product): string
    {
        $format = preg_replace("/.*?\./", '', $product);
        $image = file_get_contents($product);
        $name = md5($product);
        $path = $this->params->get('kernel.project_dir') . '/public/uploads/pictures/products/' . $name . '.' . $format;
        file_put_contents($path, $image);
        return $name . '.' . $format;
    }
}
