<?php

namespace App\Command;

use App\Entity\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Exception as ExceptionAlias;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'Exchange',
    description: 'Пet the exchange rate',
)]
class ExchangeCommand extends Command
{
    public EntityManagerInterface $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     * @throws ExceptionAlias
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $xml = simplexml_load_file('https://www.nbkr.kg/XML/daily.xml');
        foreach ($xml as $dollar) {
            $currency = $this->entityManager->getRepository(Currency::class)->findOneBy(array('code' => $dollar->attributes()->ISOCode));
            if (!$currency) {
                $currency = new Currency();
            }
            $currency->setCreatedAt(new \DateTimeImmutable($xml->attributes()->Date));
            $currency->setCode($dollar->attributes()->ISOCode);
            $currency->setValue(str_replace(',','.', $dollar->Value ));
            $this->entityManager->persist($currency);
        };

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
