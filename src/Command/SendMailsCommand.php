<?php

namespace App\Command;


use App\Repository\EmailRepository;
use App\Repository\PostRepository;
use App\Services\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

#[AsCommand(
    name: 'SendMails',
    description: 'Send mails for all users',
)]
class SendMailsCommand extends Command
{
    public EmailRepository $emails;
    public MailerInterface $mailer;
    public PostRepository $posts;
    public EntityManagerInterface $entityManager;

    /**
     * @param EmailRepository $emailRepository
     * @param MailerInterface $mailer
     * @param PostRepository $postRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EmailRepository $emailRepository, MailerInterface $mailer, PostRepository $postRepository, EntityManagerInterface $entityManager)
    {
        $this->emails = $emailRepository;
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->posts = $postRepository;
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mailer = new Mailer($this->emails, $this->mailer, $this->posts, $this->entityManager);
        $mailer->sendEmail();
        return Command::SUCCESS;
    }
}
