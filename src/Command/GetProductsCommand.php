<?php

namespace App\Command;

use App\Entity\Job;
use App\Entity\Page;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

#[AsCommand(
    name: 'GetProductsCommand',
    description: 'Add a short description for your command',
)]
class GetProductsCommand extends Command
{
    public EntityManagerInterface $entityManager;
    public ContainerBagInterface $params;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ContainerBagInterface $params
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerBagInterface $params)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $job = $this->entityManager->getRepository(Job::class)->findOneBy([]);
        $urls = $this->entityManager->getRepository(Page::class)->findAll();
        $io = new SymfonyStyle($input, $output);
        $job->setStatus('in progress');
        $this->entityManager->persist($job);
        $this->entityManager->flush();
        foreach ($urls as $url) {
            $client = HttpClient::create();

            $response = $client->request('GET', $url->getUrl());

            if ($response->getStatusCode() == 200) {
                $products = $this->getContent($response);
                $sql = "INSERT OR REPLACE INTO product (name, price, picture) VALUES ";
                $values = [];
                foreach ($products as $product) {
                    $price = explode(' ',trim($product['price']));
                    $format = preg_replace('/^.*\.(.*)$/U', '$1', $product['img']);
                    $name = md5($product['img']);
                    if ($product['img']) {
                        $path = $this->params->get('kernel.project_dir') . '/public/uploads/pictures/products/' . $name . '.' . $format;
                        $image = file_get_contents('https://enter.kg' . $product['img']);
                        file_put_contents($path, $image);
                        $picture = $name . '.' . $format;
                        $values[] = '(\'' . $product['name'] . '\', ' . $price[0] . ', \'' . $picture. '\')';
                    } else {
                        $values[] = '(\'' . $product['name'] . '\', ' . $price[0] . ', \'' . null . '\')';
                    }

                }

                $sql .= implode(',', $values);

                $this->entityManager->getConnection()->executeQuery($sql);
            }

            if ($job->getQty()) {
                $job->setQty($job->getQty() + 1);
            } else {
                $job->setQty(1);
            }

            $this->entityManager->persist($job);
            $this->entityManager->flush();

        }

        $job->setStatus('done');
        $job->setQty(0);
        $this->entityManager->persist($job);
        $this->entityManager->flush();
        $io->success('Done');

        return Command::SUCCESS;
    }

    /**
     * @param $response
     * @return array
     */
    public function getContent($response): array
    {
        $content = $response->getContent();
        $crawler = new Crawler($content);
        return $crawler->filter('.browse-view .row')->each(function ($node) {
            $name = $node->filter('.prouct_name')->text();
            $price = $node->filter('.price')->text();
            if ($node->filter('img')->attr('src') == '/images/novinka.gif' || $node->filter('img')->attr('src') == '/images/yandex.png') {
                $img = null;
            } else {
                $img = $node->filter('img')->attr('src');
            }
            return compact('img', 'name', 'price');
        });
    }
}
