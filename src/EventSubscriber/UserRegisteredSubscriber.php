<?php

namespace App\EventSubscriber;

use App\Event\UserRegisteredEvent;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class UserRegisteredSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            UserRegisteredEvent::NAME => 'onUserRegistered'
        ];
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function onUserRegistered(UserRegisteredEvent $event)
    {
        $email = (new TemplatedEmail())
            ->from('baatirbekov.aktan@gmail.com')
            ->to($event->getUser()->getEmail())
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->htmlTemplate('emails/posts.html.twig');

        $this->mailer->send($email);
    }
}
