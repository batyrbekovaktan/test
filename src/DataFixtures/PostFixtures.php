<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $post = new Post();
            $post->setTitle($faker->name);
            $post->setActive(true);
            $post->setCreatedAt(new \DateTimeImmutable());
            $post->setDescription($faker->userName);
            $manager->persist($post);
        }

        for ($i = 0; $i < 5; $i++) {
            $post = new Post();
            $post->setTitle($faker->name);
            $post->setCreatedAt(new \DateTimeImmutable());
            $post->setDescription($faker->userName);
            $manager->persist($post);
        }

        $manager->flush();
    }
}
