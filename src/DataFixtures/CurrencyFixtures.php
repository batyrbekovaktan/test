<?php

namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $currency = new Currency();
        $currency->setValue(99.4437);
        $currency->setCode('USD');
        $currency->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setValue(109.5472);
        $currency->setCode('EUR');
        $currency->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setValue(0.9362);
        $currency->setCode('RUB');
        $currency->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($currency);

        $currency = new Currency();
        $currency->setValue(0.1968);
        $currency->setCode('KZT');
        $currency->setCreatedAt(new \DateTimeImmutable());
        $manager->persist($currency);

        $manager->flush();
    }
}
