<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 6; $i++) {
            $category = new Category();
            $category->setName($faker->word);
            $category->setDescription($faker->paragraph(10));
            $manager->persist($category);

            $brand = new Brand();
            $brand->setName($faker->word);
            $brand->setCategory($category);
            $brand->setDescription($faker->paragraph(10));
            $manager->persist($brand);
        }
        $manager->flush();
    }
}
