<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Image;
use App\Entity\Mini;
use App\Entity\Product;
use App\ImageOptimizer;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class ProductFixtures extends Fixture
{
    public CategoryRepository $categoryRepository;
    public ProductRepository $productRepository;
    private UserRepository $userRepository;
    private EntityManagerInterface $entityManager;
    private ContainerBagInterface $params;
    private ImageOptimizer $imageOptimizer;

    /**
     * @param ProductRepository $productRepository
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param CategoryRepository $categoryRepository
     * @param ContainerBagInterface $params
     */
    public function __construct
    (
        ImageOptimizer         $imageOptimizer,
        ProductRepository      $productRepository,
        UserRepository         $userRepository,
        EntityManagerInterface $entityManager,
        CategoryRepository     $categoryRepository,
        ContainerBagInterface  $params)
    {
        $this->imageOptimizer = $imageOptimizer;
        $this->params = $params;
        $this->categoryRepository = $categoryRepository;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    private function getImage(int $image_number): string
    {
        $path = $this->params->get('kernel.project_dir') . '/src/Resources/images/' . $image_number . '.jpg';
        $image_name = md5($path) . '.jpg';
        $image = file_get_contents($path);
        $new_path = $this->params->get('kernel.project_dir') . '/public/uploads/pictures/products/' . $image_name;
        file_put_contents($new_path, $image);
        return $image_name;
    }


    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $category_sql = 'SELECT id from `category` ORDER BY random() LIMIT 1';
        $faker = Factory::create();
        $user_sql = 'SELECT id from `user` ORDER BY random() LIMIT 1';


        $names = ['Range Rover',
            'Mercedes-Benz',
            'Mazda',
            'Toyota'];

        for ($i = 0; $i < 24; $i++) {
            $product = new Product();
            $category = $this->entityManager->getConnection()->executeQuery($category_sql)->fetchAllAssociative()[0]['id'];
            $product->setName($names[array_rand($names)]);
            $product->setCategory($this->categoryRepository->find($category));
            $product->setPicture($this->getImage(rand(1, 9)));
            $product->setPrice(rand(1000, 10000));
            $product->setDescription($faker->paragraph(10));
            $manager->persist($product);

            for ($c = 0; $c < 3; $c++) {
                $new_picture = $this->getImage(rand(1, 9));
                $new_image = new Image();
                $new_image->setImage($new_picture);
                $new_image->setProduct($product);
                $this->entityManager->persist($new_image);
                $this->imageOptimizer
                    ->resize(
                        $this->params->get('pictures_directory') . '/products/' . $new_picture,
                        $this->params->get('pictures_directory') . '/resize/' . $new_picture,
                    );
                $mini = new Mini();
                $mini->setPicture($new_picture);
                $mini->setImage($new_image);
                $mini->setType(preg_replace("/.*?\./", '', $new_picture));
                $this->entityManager->persist($mini);
            }

            for ($j = 0; $j < 3; $j++) {
                $comment = new Comment();
                $user = $this->entityManager->getConnection()->executeQuery($user_sql)->fetchAllAssociative()[0]['id'];
                $comment->setCreatedAt(new DateTimeImmutable());
                $comment->setUser($this->userRepository->find($user));
                $comment->setProduct($product);
                $comment->setContent($faker->paragraph(2));
                $manager->persist($comment);
            }
            $manager->flush();
        }
    }
}
