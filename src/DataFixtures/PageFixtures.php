<?php

namespace App\DataFixtures;

use App\Entity\Job;
use App\Entity\Page;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PageFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $job = new Job();
        $job->setName('get products');
        $job->setQty(0);
        $job->setStatus('to start');
        $manager->persist($job);
        $page = new Page();
        $page->setUrl('https://enter.kg/videokarty_bishkek');
        $manager->persist($page);
        $page = new Page();
        $page->setUrl('https://enter.kg/klaviatury-i-myshki_bishkek/myshki_bishkek');
        $manager->persist($page);
        $page = new Page();
        $page->setUrl('https://enter.kg/operativnaya-pamyat_bishkek/ddr-4_bishkek');
        $manager->persist($page);
        $page = new Page();
        $page->setUrl('https://enter.kg/klaviatury-i-myshki_bishkek/klaviatury_bishkek');
        $manager->persist($page);
        $manager->flush();
    }
}
