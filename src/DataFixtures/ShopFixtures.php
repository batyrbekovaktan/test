<?php

namespace App\DataFixtures;

use App\Entity\Shop;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ShopFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 2; $i++) {
            $shop = new Shop();
            $shop->setName($faker->name);
            $shop->setAddress($faker->address);
            $shop->setPhone($faker->phoneNumber);
            $manager->persist($shop);
        }

        $manager->flush();
    }
}
