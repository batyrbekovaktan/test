<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setName($faker->name);
            $user->setIsVerified(true);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(password_hash("password", PASSWORD_DEFAULT));
            $manager->persist($user);
        }

        $user = new User();
        $user->setEmail('admin@admin.com');
        $user->setName('admin');
        $user->setIsVerified(true);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword(password_hash("password", PASSWORD_DEFAULT));
        $manager->persist($user);

        $manager->flush();
    }
}
