<?php

namespace App\Services;

use App\Repository\PostRepository;

class PostsAll
{
    public PostRepository $postRepository;
    public array $posts;

    /**
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
        $this->posts = $postRepository->findAllActive();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->posts;
    }
}