<?php

namespace App\Services;

use App\Repository\SliderImageRepository;

class SliderImagesAll
{
    public SliderImageRepository $sliderImageRepository;
    public array $images;

    /**
     * @param SliderImageRepository $sliderImageRepository
     */
    public function __construct(SliderImageRepository $sliderImageRepository)
    {
        $this->sliderImageRepository = $sliderImageRepository;
        $this->images = $this->getAll();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $items = $this->sliderImageRepository->findAll();

        $images = [];
        foreach ($items as $item){
            $images[] = '/uploads/pictures/slider/'. $item->getImage();
        }

        return $images;
    }
}