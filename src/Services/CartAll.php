<?php

namespace App\Services;

use App\Repository\CurrencyRepository;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Session\Session;

class CartAll
{
    public PostRepository $postRepository;
    public Session $session;
    private CurrencyRepository $currencyRepository;

    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
        $this->session = new Session();
    }

    /**
     * @return mixed
     */
    public function getAll(): mixed
    {
        return $this->session->get('products');
    }

    /**
     * @return string
     */
    public function getTotal(): string
    {
        $total = 0;

        if ($this->session->get('products')) {
            foreach ($this->session->get('products') as $product) {
                $total += $product['price'] * $product['qty'];
            }
        }

        $locale = $this->session->get('_locale');

        if(!$locale) {
            $locale = 'en';
        }

        return match ($locale) {
            "en" => round($total / $this->currencyRepository->findOneBy(['code' => 'USD'])->getValue(), 2) . ' $',
            "fr" => round($total / $this->currencyRepository->findOneBy(['code' => 'EUR'])->getValue(), 2) . ' €',
            "ru" => round($total / $this->currencyRepository->findOneBy(['code' => 'RUB'])->getValue(), 2) . ' ₽',
            "kz" => round($total / $this->currencyRepository->findOneBy(['code' => 'KZT'])->getValue(), 2) . ' ₸',
            "kg" => $total . ' C',
        };
    }
}