<?php

namespace App\Services;

use App\Repository\CategoryRepository;

class CategoriesAll
{
    public CategoryRepository $categoryRepository;
    public array $categories;

    /**
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categories = $this->getAll();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->categoryRepository->findAll();
    }
}