<?php

namespace App\Services;

use App\Repository\ShopRepository;

class ShopsAll
{
    public ShopRepository $shopRepository;
    public array $shops;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->shops = $this->getAll();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->shopRepository->findAll();
    }
}