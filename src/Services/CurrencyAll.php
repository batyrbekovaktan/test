<?php

namespace App\Services;

use App\Repository\CurrencyRepository;

class CurrencyAll
{
    public CurrencyRepository $currencyRepository;
    public array $currencies;

    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
        $this->currencies = $this->getAll();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        return $this->currencyRepository->findAll();
    }
}