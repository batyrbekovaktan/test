<?php

namespace App\Services;

use App\Repository\EmailRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class Mailer
{
    public array $emails;
    public MailerInterface $mailer;
    public array $posts;
    public EntityManagerInterface $entityManager;

    /**
     * @param EmailRepository $emailRepository
     * @param MailerInterface $mailer
     * @param PostRepository $postRepository
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EmailRepository $emailRepository, MailerInterface $mailer, PostRepository $postRepository, EntityManagerInterface $entityManager)
    {
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->emails = $emailRepository->findAll();
        $this->posts = $postRepository->findAllActive();
    }

    /**
     * @return void
     * @throws TransportExceptionInterface
     */
    public function sendEmail()
    {
        if (count($this->posts) > 0) {
            foreach ($this->emails as $email) {

                $email = (new TemplatedEmail())
                    ->from('baatirbekov.aktan@gmail.com')
                    ->to($email->getEmail())
                    ->subject('Time for Symfony Mailer!')
                    ->text('Sending emails is fun again!')
                    ->htmlTemplate('emails/posts.html.twig');

                $this->mailer->send($email);
            }

            foreach ($this->posts as $post) {
                $post->setActive(false);
                $this->entityManager->persist($post);

                $this->entityManager->flush();
            }
        }
    }
}