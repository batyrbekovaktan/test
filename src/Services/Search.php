<?php

namespace App\Services;

use IntlChar;
use Phpml\Classification\KNearestNeighbors;

class Search
{
    /**
     * @param $word
     * @return array|mixed
     */
    static function search($word): mixed
    {
        $cars = [
            'Range Rover' =>
                [
                    'rr', 'r r', 'range', 'renge', 'raange', 'rang', 'рэндж', 'рендж', 'рендж ровер', 'ровер', 'range rover', 'rangerover'
                ],
            'Mercedes-Benz' =>
                [
                    'mb', 'mersedes', 'mers', 'mercedes', 'mersedec benz', 'benz', 'мерс', 'мерседес', 'мерседес бэез', 'бенс', 'mersedes-bens',
                    'mersedes bens', 'mercedes bens', 'mersdes'
                ],
            'Mazda' =>
                [
                    'mazda', 'masda', 'мазда', 'масда', 'maazda', 'masd', 'mazd', 'маазда'
                ],
            'Toyota' =>
                [
                    'toyota', 'тайота', 'таета', 'таёта', 'тойота', 'touta', 'touota', 'tayota'
                ]
        ];

        $samples = [];
        $labels = [];

        foreach ($cars as $key => $values) {
            foreach ($values as $value) {
                $labels[] = $key;
                $chars = preg_split('//u', $value, NULL, PREG_SPLIT_NO_EMPTY);
                $code = [];
                foreach ($chars as $char) {
                    $code[] = IntlChar::ord($char);
                }

                if (count($code) < 13) {
                    for ($i = count($code); $i < 13; $i++) {
                        $code[] = 0;
                    }
                }

                $samples[] = $code;
            }
        }

        $classifier = new KNearestNeighbors();
        $classifier->train($samples, $labels);


        $str = preg_split('//u', substr(mb_strtolower($word), 0, 13), NULL, PREG_SPLIT_NO_EMPTY);
        $array = [];
        foreach ($str as $char) {
            $array[] = IntlChar::ord($char);
        }

        if (count($array) < 13) {
            for ($i = count($array); $i < 13; $i++) {
                $array[] = 0;
            }
        }

        return $classifier->predict($array);
    }


}