<?php

namespace App\Twig;

use App\Repository\CurrencyRepository;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ProductExtension extends AbstractExtension
{
    private Session $session;
    private CurrencyRepository $currencyRepository;

    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
        $this->session = new Session();
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'formatPrice']),
        ];
    }


    /**
     * @param $number
     * @return string
     */
    public function formatPrice($number): string
    {
        $locale = $this->session->get('_locale');

        if (!$locale) {
            $locale = 'en';
        }

        $price = intval($number);
        switch ($locale) {
            case 'en':
                $price = round($price / floatval($this->currencyRepository->findOneBy(['code' => 'USD'])->getValue()), 2);
                $price = $price . ' $';
                break;
            case 'ru':
                $price = round($price / floatval($this->currencyRepository->findOneBy(['code' => 'RUB'])->getValue()), 2);
                $price = $price . ' ₽';
                break;
            case 'fr':
                $price = round($price / floatval($this->currencyRepository->findOneBy(['code' => 'EUR'])->getValue()), 2);
                $price = $price . ' €';
                break;
            case 'kz':
                $price = round($price / floatval($this->currencyRepository->findOneBy(['code' => 'KZT'])->getValue()), 2);
                $price = $price . ' ₸';
                break;
            case 'kg':
                $price = $price . ' C';
                break;
        }

        return $price;
    }
}
