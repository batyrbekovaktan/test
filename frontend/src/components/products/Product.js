import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {isAuth} from "../../utils";
import {get, post} from "../../request";
import Box from "@mui/material/Box";
import './Product.css';
import LinearProgress from "@mui/material/LinearProgress";
import Grid from "@mui/material/Grid";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import FavoriteIcon from "@mui/icons-material/Favorite";
import Button from "@mui/material/Button";

const Product = () => {
    const [showAlert, setShowAlert] = useState(false);
    const {id} = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const [product, setProduct] = useState(null);
    const [error, setError] = useState(null);

    useEffect(() => {
        setIsLoading(true);
        const response = get("/products/" + id);
        response.then(data => {
            setProduct(data);
        })
        setIsLoading(false);
    }, []);

    const imgs = document.querySelectorAll('.img-select a');
    const imgBtns = [...imgs];
    let imgId = 1;

    imgBtns.forEach((imgItem) => {
        imgItem.addEventListener('click', (event) => {
            event.preventDefault();
            imgId = imgItem.dataset.id;
            slideImage();
        });
    });

    function slideImage(){
        const displayWidth = document.querySelector('.img-showcase img:first-child').clientWidth;

        document.querySelector('.img-showcase').style.transform = `translateX(${- (imgId - 1) * displayWidth}px)`;
    }

    window.addEventListener('resize', slideImage);

    return (
        <>

            {isLoading &&
                <Box sx={{width: '100%'}}>
                    <LinearProgress/>
                </Box>
            }
            {product &&
                <div>
                    <div className="card-wrapper">
                        <div className="card">
                            <div className="product-imgs">
                                <div className="img-display">
                                    <div className="img-showcase">
                                        <img
                                            src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_1.jpg"
                                            alt="shoe image"/>
                                            <img
                                                src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_2.jpg"
                                                alt="shoe image"/>
                                                <img
                                                    src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_3.jpg"
                                                    alt="shoe image"/>
                                                    <img
                                                        src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_4.jpg"
                                                        alt="shoe image"/>
                                    </div>
                                </div>
                                <div className="img-select">
                                    <div className="img-item">
                                        <a href="#" data-id="1">
                                            <img
                                                src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_1.jpg"
                                                alt="shoe image"/>
                                        </a>
                                    </div>
                                    <div className="img-item">
                                        <a href="#" data-id="2">
                                            <img
                                                src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_2.jpg"
                                                alt="shoe image"/>
                                        </a>
                                    </div>
                                    <div className="img-item">
                                        <a href="#" data-id="3">
                                            <img
                                                src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_3.jpg"
                                                alt="shoe image"/>
                                        </a>
                                    </div>
                                    <div className="img-item">
                                        <a href="#" data-id="4">
                                            <img
                                                src="https://fadzrinmadu.github.io/hosted-assets/product-detail-page-design-with-image-slider-html-css-and-javascript/shoe_4.jpg"
                                                alt="shoe image"/>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div className="product-content">
                                <h2 className="product-title">{product.name}</h2>
                                <div className="product-rating">
                                    {/*<i className="fas fa-star"></i>*/}
                                    {/*<i className="fas fa-star"></i>*/}
                                    {/*<i className="fas fa-star"></i>*/}
                                    {/*<i className="fas fa-star"></i>*/}
                                    {/*<i className="fas fa-star-half-alt"></i>*/}
                                    {/*<span>4.7(21)</span>*/}
                                </div>

                                <div className="product-price">
                                    <p className="last-price">Old Price: <span>${product.price * 2}</span></p>
                                    <p className="new-price">New Price: <span>$ {product.price}</span></p>
                                </div>

                                <div className="product-detail">
                                    <h2>about this item: </h2>
                                    <p>{product.description}</p>
                                    <ul>
                                        <li>Color: <span>Black</span></li>
                                        <li>Available: <span>in stock</span></li>
                                        <li>Category: <span>{product.category.name}</span></li>
                                        <li>Shipping Area: <span>All over the world</span></li>
                                        <li>Shipping Fee: <span>Free</span></li>
                                    </ul>
                                </div>

                                <div className="purchase-info">
                                    <Button variant="contained"> Add to Cart</Button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    );
};

export default Product;
