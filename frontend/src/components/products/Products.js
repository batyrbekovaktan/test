import React, {useState, useEffect} from 'react';
import Box from '@mui/material/Box';
import {get, post} from '../../request';
import {isAuth} from "../../utils";
import Grid from "@mui/material/Grid";
import './Products.css';
import {CircularProgress} from "@mui/material";
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import FavoriteIcon from '@mui/icons-material/Favorite';
import {Link as RouterLink} from "react-router-dom";
import Link from "@mui/material/Link";
import Pagination from "../pagination/Pagination";
import Button from "@mui/material/Button";


const Products = () => {
    const [isLoading, setIsLoading] = useState(false);
    // const [isLoading2, setIsLoading2] = useState(false);
    const [products, setProducts] = useState([]);
    // const token = isAuth();
    // const {t} = useTranslation();
    // const [error, setError] = useState(null);

    const [currentPage, setCurrentPage] = useState(1);
    const [productsPerPage] = useState(9);

    const lastProductIndex = currentPage * productsPerPage;
    const firstProductIndex = lastProductIndex - productsPerPage;
    const currentProducts = products.slice(firstProductIndex, lastProductIndex);

    const paginate = pageNumber => setCurrentPage(pageNumber)


    useEffect(() => {
        setIsLoading(true);
        const response = get("/products/");
        response.then(data => {
            setProducts(data);
            console.log(data)
            setIsLoading(false);
        })
        // setIsLoading2(true);
    }, []);

    return (
        <>
            <div className="container products">
                {isLoading &&
                    <Box sx={{width: '100%'}}>
                        <CircularProgress/>
                    </Box>
                }
                {products &&
                    <Grid container spacing={{xs: 4, md: 10}} columns={{xs: 4, sm: 8, md: 12}}>
                        {currentProducts.map(product =>
                            <Grid item xs={2} sm={4} md={4} key={product.id}>
                                <div className="product-card">
                                    {/*<div className="badge">Hot</div>*/}
                                    <div className="product-tumb">
                                        <img
                                            src={'/uploads/pictures/products/' + product.picture}
                                            alt=""/>


                                        {/*<img src="https://issaplus.ru/wa-data/public/photos/01/55/5501/5501.530x509.jpg" alt=""/>*/}
                                    </div>
                                    <div className="product-details">
                                        <span className="product-catagory">{product.category.name}</span>
                                        <h4><a href="">{product.name}</a></h4>
                                        <div className="product-bottom-details">
                                            <div className="product-price"><small>${product.price * 2}</small>${product.price}</div>
                                            <div className="product-links">
                                                {/*<Link component={RouterLink} to={`/users/${product.id}`}>{product.name}</Link>*/}
                                                <Button component={RouterLink} to={`/products/${product.id}`} variant="contained" color="primary">Show</Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Grid>
                        )}
                    </Grid>
                }
                <Pagination
                    usersPerPage={productsPerPage}
                    totalUsers={products.length}
                    paginate={paginate}
                    currentPage={currentPage}
                />
            </div>
        </>
    );
};

export default Products;