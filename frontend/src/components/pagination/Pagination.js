import React from 'react';
import './Pagination.css'

const Pagination = ({usersPerPage, totalUsers, paginate, currentPage}) => {
    const pageNumber = [];

    for (let i = 1; i <= Math.ceil(totalUsers / usersPerPage); i++) {
        pageNumber.push(i);
    }

    return (
        <div>
            <ul className="pagination">
                {
                    pageNumber.map(number => (
                        <li className="page-item" key={number}>
                            <a
                                className={`page-link ${currentPage === number ? "active" : ""}`}
                                onClick={() => paginate(number)}
                            >{number}</a>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
};

export default Pagination;
