import {Link} from "react-router-dom";
import './Categories.css';
import React, {useState, useEffect} from 'react';
import {get} from '../../request';
import {isAuth} from "../../utils";
import {CircularProgress} from "@mui/material";
// import {useTranslation} from "react-i18next";



const Categories = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [categories, setCategories] = useState(null);
    // const { t } = useTranslation();
    // const token = isAuth();

    useEffect(() => {
        const response = get("/categories/");
        response.then(data => {
            console.log(data)
                setCategories(data);

        })
        setIsLoading(false);
    }, [isLoading === true]);


    return (
        <>
            {isLoading && <CircularProgress />}
            {categories &&
                <div className="content-wrapper">
                    {categories.map( category =>
                        <div key={category.id} className="news-card">
                            <Link to={`/category/${category.id}`} className="news-card__card-link"> </Link>
                            <div className="news-card__text-wrapper">
                                <h2 className="news-card__title"> {category.name}</h2>
                                <div className="news-card__details-wrapper">
                                    <p className="news-card__excerpt" dangerouslySetInnerHTML={{__html: category.description}}/>
                                    <Link to={`/category/${category.id}`} className="news-card__read-more">Read more</Link>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            }
        </>
    );
};

export default Categories;
