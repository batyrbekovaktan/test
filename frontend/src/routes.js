import Home from "./components/common/Home";
import Categories from "./components/categories/Categories";
import Products from "./components/products/Products";
import Product from "./components/products/Product";


const routes = [
    {
        path: '/',
        name: 'Home',
        element: <Home/>,
        auth: false,
        includeNav: true,
    },
    {
        path: '/categories',
        name: 'Categories',
        element: <Categories/>,
        auth: false,
        includeNav: true,
    },
    {
        path: '/products',
        name: 'Products',
        element: <Products/>,
        auth: false,
        includeNav: true,
    },
    {
        path: '/products/:id',
        name: 'Product',
        element: <Product/>,
        auth: false,
        props: true,
        includeNav: false,
    },

    // {
    //     path: '/articles',
    //     name: 'Articles',
    //     element: <Articles/>,
    //     auth: true,
    //     includeNav: true,
    // },
    // {
    //     path: '/articles/create',
    //     name: 'Create article',
    //     element: <CreateArticle/>,
    //     auth: true,
    //     includeNav: true,
    // },
    // {
    //     path: '/articles/:articleId',
    //     name: 'Article',
    //     element: <ShowArticle/>,
    //     auth: true,
    //     includeNav: false,
    // },
    // {
    //     path: '/articles/edit/:articleId',
    //     name: 'Article edit',
    //     element: <EditArticle/>,
    //     auth: true,
    //     includeNav: false,
    // }
];

export default routes;
