$(() => {
    $('.page-link').on('click', function (e) {
        e.preventDefault();
        getAllProduct(e)
    })

    const getAllProduct = e => {
        const element = e.currentTarget;
        const data = $(element).attr('href');

        $.ajax({
            method: 'GET',
            url: `/all${data}`
        }).done(successResponse => {
            $('#paginate_products').remove();
            $('#all_paginate_products').append(successResponse)
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                getProducts(e)
            })
        }).fail(e => {
            console.log('no')
        });
    }


    const getProducts = e => {
        const element = e.currentTarget;
        const data = $(element).attr('href');

        $.ajax({
            method: 'GET',
            url: `${data}`
        }).done(successResponse => {
            $('#paginate_products').remove();
            $('#all_paginate_products').append(successResponse)
        }).fail(e => {
            console.log('no')
        });
    }

    $(document).on('click', '#back_to_products', function (e) {
        e.preventDefault();
        getAllProduct(e)
    })


    $(document).on('click', '#search_product', function (e) {
        e.preventDefault();
        searchProducts(e)
    })

    const searchProducts = e => {
        const element = e.currentTarget;
        const data = $(element).parent().parent().serialize();

        $.ajax({
            method: 'POST',
            url: `/products/search`,
            data: data
        }).done(successResponse => {
            $('#paginate_products').remove();
            $('#all_paginate_products').append(successResponse)
            $(document).on('click', '.page-link', function (e) {
                e.preventDefault();
                getProducts(e)
            })
        }).fail(e => {
            console.log('no')
        });
    }

    $(document).on('click', '#get_new_products', function (e) {
        e.preventDefault();
        getNewProducts(e)
    })

    const getNewProducts = e => {

        $.ajax({
            method: 'POST',
            url: `/admin/job/status`
        }).done(successResponse => {
            if (successResponse.status !== 'in progress') {
                $.ajax({
                    method: 'POST',
                    url: `/admin/job/store`
                }).done(successResponse => {
                    $('#spinner').replaceWith($(`<div id="spinner_div" class="spinner-border my-4" role="status">
              <span class="visually-hidden">Loading...</span>
              </div>`));
                    progress(e)
                }).fail(e => {
                    console.log('no')
                });
            }
        }).fail(e => {
            console.log('no')
        });
    }

    const progress = e => {
        $.ajax({
            method: 'POST',
            url: `/admin/job/status`
        }).done(successResponse => {
            const percent = parseInt(successResponse.qty) / 4 * 100;

            element(percent)
            if (successResponse.status !== 'done') {
                setTimeout(progress, 3000);
            } else {
                element(100);
                $('#spinner_div').remove();
            }
        }).fail(e => {
            console.log('no')
        });
    }

    function element(percent) {
        return $('.progress').replaceWith($(`<div class="progress mt-5">
                <div style="width: ${percent}%;" class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>`));
    }

});