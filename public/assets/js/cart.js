(function($) {
    // $(document).on('click', '#cart_save_btn', function (e) {
    //     e.preventDefault();
    //     console.log('hello')
    // })
    //
    // const deleteAll = e => {
    //     const element = e.currentTarget;
    //     const institutionId = $(element).data('institution-id');
    //     const data = $(element).parent().serialize();
    //
    //     $.ajax({
    //         method: 'DELETE',
    //         url: `/deleteAll/${institutionId}`,
    //         data: data
    //     }).done(successResponse => {
    //         $('.cart-dishes').remove();
    //         $('#total').text('0 сом')
    //     }).fail(e => {
    //         console.log('no')
    //     });
    // };

    $(document).on('click', '.cart_product_delete', function (e) {
        e.preventDefault();
        deleteProduct(e);
    })

    const deleteProduct = e => {
        const element = e.currentTarget;
        const productId = $(element).data('product-id');
        const data = $(element).parent().serialize();

        $.ajax({
            method: 'POST',
            url: `/cart/delete`,
            data: data
        }).done(successResponse => {
            total();
            $(`.cart-${productId}`).remove();
        }).fail(e => {
            console.log('no')
        });
    };

    $(document).on('click', '#add_product_btn', function (e) {
        e.preventDefault();
        addProduct(e);
    })

    const addProduct = e => {
        const element = e.currentTarget;
        const data = $(element).parent().serialize();
        const productId = $(element).data('product-id');
        const div = $(`#cart-${productId}`);

        $.ajax({
            method: 'POST',
            url: `/cart/store`,
            data: data
        }).done(successResponse => {
            if ($(div).length) {
                $(div).replaceWith(successResponse)
            } else {
                $(`#cart_products`).prepend(successResponse);
            }
            total();
        }).fail(e => {
            console.log('no')
        });
    }

    $(document).on('click', '.cart_product_update', function (e) {
        e.preventDefault();
        update(e);
    })

    const update = e => {
        const element = e.currentTarget;
        const data = $(element).parent().parent().serialize();
        const productId = $(element).data('product-id');
        const div = $(`.cart-${productId}`);
        console.log(data)

        $.ajax({
            method: 'POST',
            url: `/cart/update`,
            data: data
        }).done(successResponse => {
            total();
            $(div).replaceWith(successResponse);
        }).fail(e => {
            console.log('no')
        });
    }


    const total = e => {
        $.ajax({
            method: 'GET',
            url: `/cart/total`,
        }).done(successResponse => {
            $('.cart_total').text(successResponse.total)
        }).fail(e => {
            console.log('no')
        });
    }

})(jQuery);
